---
enableTitleFooter: false
title: OpenSilver
theme: league
---

## Opensilver

<small> Łukasz Łysyganicz | llysyganicz@pgs-soft.com </small>

---

## Co to jest openSilver?

Otwarta reimplementacja Silverlighta <!-- .element: class="fragment" data-fragment-index="1" -->

<table class="fragment">
  <thead>
    <th></th>
    <th>Silverlight</th>
    <th>OpenSilver</th>
  </thead>
  <tbody>
    <tr>
      <td>Wymaga pluginu</td>
      <td>Tak</td>
      <td>Nie</td>
    </tr>
    <tr>
      <td>Wersja C#</td>
      <td>6</td>
      <td>6+</td>
    </tr>
    <tr>
      <td>Komponenty od firm trzecich (Telerik, SyncFusion, itd.)</td>
      <td>Pełna obsługa</td>
      <td>Występują błędy</td>
    </tr>
  </tbody>
</table>

---

## Dlaczego OpenSilver?

<ul>
  <li>Aktualizacja istniejącej aplikacji Silverlight</li>
  <li class="fragment">Przeniesienie aplikacji WPF do Weba</li>
  <li class="fragment">Przeniesienie aplikacji UWP do Weba</li>
</ul>

--

<h2 class="r-fit-text">Demo</h2>

---

## Linki

- https://opensilver.net
- https://www.dotnetrocks.com/?show=1769

---

## Dziękuję