---
enableTitleFooter: false
title: Flutter - wprowadzenie
---

## Flutter - wprowadzenie

<small>Łukasz Łysyganicz | llysyganicz@outlook.com</small>

---

## Agenda

- Instalacja SDK
- Konfiguracja IDE/edytora
- Pierwszy projekt
- Zarządzanie stanem aplikacji

---

## Instalacja SDK

Wymagania: zainstalowane Android SDK (Android) lub XCode (iOS)

Sposoby instalacji:
<ul class="fragment">
  <li>Wg instrukcji ze strony <a href="https://flutter.dev">flutter.dev</a></li>
  <li class="fragment">Android Studio, IntelliJ</li>
  <li class="fragment">Repozytoria:
    <ul>
      <li>Linux</li>
      <li>Homebrew dla Maca</li>
      <li>scoop lub chocolatey dla Windowsa</li>
    </ul>
  </li>
</ul>

---

## Konfiguracja Android Studio / IntelliJ

Pluginy: Flutter, Dart (instalowany jako zależność do fluttera), coś do kolorowania nawiasów

--

## Konfiguracja Visual Studio Code

* Dart
* Flutter
* Pubspec Assist
* Rainbow Brackets

---

## Pierwszy projekt

<pre class="fragment"><code>flutter doctor</code></pre>
<pre class="fragment"><code>flutter upgrade (--force)</code></pre>
<pre class="fragment"><code>flutter create projectname</code></pre>

---

## Zarządzanie stanem aplikacji

<ul>
  <li class="fragment"><code>setState()</code></li>
  <li class="fragment">Business Logic Component (BLoC)</li>
  <li class="fragment">Provider</li>
  <li class="fragment">Redux - dla zaznajomionych z Reactem</li>
</ul>

--

## `setState()`

```dart
void _incrementCounter {
  setState(() {
    ++_counter;
  });
}
```

--

## BLoC

* Google IO'18
* Reactive Programming:
  * `StreamController`
  * Sink - input
  * Stream - output
  * `StreamBulider` - widget

--

## Provider

https://pub.dev/packages/provider
> A mixture between dependency injection (DI) and state management
<p>
  Typy providerów:
  <ul class="fragment">
    <li>ListenableProvider</li>
    <li>ChangeNotifierProvider</li>
    <li>ValueListenableProvider</li>
    <li>StreamProvider</li>
    <li>FutureProvider</li>
  </ul>
</p>

---

## Dziękuję