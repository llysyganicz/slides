## Budowa aplikacji Xamarin.Forms z wykorzystaniem języka F# i wzorca MVU

<small>Łukasz Łysyganicz | llysyganicz@outlook.com</small>

---

## Agenda

- Wprowadzenie do MVU
- Biblioteka Fabulous
- Podstawowa struktura aplikacji
- Demo

---

## Elm architecture

<img src="https://elmbridge.github.io/curriculum/images/elm-architecture-4.jpeg" height="60%" width="60%">

<small>Źródło: https://elmbridge.github.io/curriculum/images/elm-architecture-4.jpeg</small>

--

## Biblioteka Elmish

@quote[`-ish` a suffix used to convey the sense of “having some characteristics of"]

@quote[Elmish implements core abstractions that can be used to build Fable applications following the “model view update” style of architecture, as made famous by Elm.]

<small>Źródło: https://elmish.github.io/elmish/</small>

---

## Biblioteka Fabulous

Elmish dla Xamarin.Forms

---

## Podstawowa struktura aplikacji

<ul>
  <li class="fragment">Model</li>
  <li class="fragment">Wiadomości</li>
  <li class="fragment">Funkcja init</li>
  <li class="fragment">Funkcja update</li>
  <li class="fragment">Widok</li>
  <li class="fragment">Połączenie całości</li>
</ul>

--

## Model

```fsharp
type Model = 
  { Count : int 
    Step : int
    TimerOn: bool }
```

--

## Wiadomości

```fsharp
type Msg = 
  | Increment 
  | Decrement 
  | Reset
  | SetStep of int
  | TimerToggled of bool
  | TimedTick
```

--

## Funkcja `init`

```fsharp
let initModel () = { Count = 0; Step = 1; TimerOn=false }

let init () = initModel () , Cmd.none
```

--

## Funkcja `update`

```fsharp
let update msg model =
  match msg with
  | Increment -> { model with Count = model.Count +   model.Step }, Cmd.none
  | Decrement -> { model with Count = model.Count -   model.Step }, Cmd.none
  | Reset -> init ()
  | SetStep n -> { model with Step = n }, Cmd.none
  | TimerToggled on -> { model with TimerOn = on }, (if on  then timerCmd() else Cmd.none)
  | TimedTick -> if model.TimerOn then { model with Count =   model.Count + model.Step }, timerCmd() else model, Cmd.none 
```

--

## Widok

```fsharp
let view (model: Model) dispatch =  
  View.ContentPage(
    content=View.StackLayout(padding=30.0,verticalOptions = LayoutOptions.Center,
      children=[ 
        // If you want the button to disappear when in the initial condition then use this:
        yield View.Label(text = sprintf "%d" model.Count, horizontalOptions = LayoutOptions.Center, widthRequest=200.0, horizontalTextAlignment=TextAlignment.Center)
        yield View.Button(text="Increment", command= (fun () -> dispatch Increment))
        yield View.Button(text="Decrement", command= (fun () -> dispatch Decrement)) 
        yield View.StackLayout(padding=20.0, orientation=StackOrientation.Horizontal, horizontalOptions=LayoutOptions.Center,
                        children = [ View.Label(text="Timer")
                                     View.Switch(isToggled=model.TimerOn, toggled=(fun on -> dispatch (TimerToggled on.Value))) ])
        yield View.Slider(minimumMaximum=(0.0, 10.0), value= double model.Step, valueChanged=(fun args -> dispatch (SetStep (int (args.NewValue + 0.5)))))
        yield View.Label(text=sprintf "Step size: %d" model.Step, horizontalOptions=LayoutOptions.Center) 
        //if model <> initModel () then 
        yield View.Button(text="Reset", horizontalOptions=LayoutOptions.Center, command=(fun () -> dispatch Reset), canExecute = (model <> initModel () ))
      ]))   
```

--

## Połączenie całości

```fsharp
let program = 
  Program.mkProgram init update view 
  |> Program.withConsoleTrace

type CounterApp () as app = 
  inherit Application ()

  let runner = App.program |> Program.runWithDynamicView app
```

---

## Demo

<ul>
  <li>
    Prosta aplikacja:
    <ul>
      <li><pre><code class="hljs">dotnet new -i Fabulous.Templates</code></pre></li> 
      <li><pre><code class="hljs">dotnet new fabulous-app -o FabulousDemo</code></pre></li>
    </ul>
  </li>
  <li class="fragment">Bardziej rozbudowana aplikacja: <a href="https://github.com/llysyganicz/NotesApp">https://github.com/llysyganicz/NotesApp</a></li>
</ul>

---

## Linki

* Elmish: https://elmish.github.io/elmish/
* SAFE Stack: https://safe-stack.github.io/
* Fabulous: https://fsprojects.github.io/Fabulous/
* Awesome Fabulous: https://github.com/jimbobbennett/Awesome-Fabulous

---

## Dziękuję

### Pytania?